Patient Initials:    BSA
Patient ID:          3010991292
Study:               Hjerne PET/MR FET dyn
Scan Protocol:       BUF_327_15_DCE_DSC
Scanner:             mMR
Tracking Successful: 1
Patient Sedated:     0
Patient Motion:      1
Only Exam of Brain:  0
PET Duration:        40
Notes:               
BUF med neuroakse dvs. lejet k�rte frem og tilbage for at rygs�jlen kunne skannes. F�rst en lokaliser af hele ryggen, derefter hjerne unders�gelser i 40 min, og efterf�lgende unders�gelseraf ryggen.
Lejet k�rte frem og tilbage fra ca. 12:49-12:50. Lejet rykkede ved 12:51:10 og igen ved 12:53:40.
Den f�rste store bev�gelse ved 12:55 var da Jakub gik ind og indjicerede PET tracer.
