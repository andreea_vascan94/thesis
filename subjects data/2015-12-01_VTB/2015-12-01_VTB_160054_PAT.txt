Patient Initials:    VTB
Patient ID:          0206037091
Study:               Hjerne PET/MR FET dyn
Scan Protocol:       BUF_327_15_DCE_DSC
Scanner:             mMR
Tracking Successful: 1
Patient Sedated:     0
Patient Motion:      1
Only Exam of Brain:  1
PET Duration:        40
Notes:               
12 year old boy, mom stayed inside the scanner room.
Slow drift during the whole scan up to 8mm.
Small movements at 16:35:45, 16:36:45 and 16:43:50 coincident with the injection of contrast fluid.

